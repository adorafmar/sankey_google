var config = {
  development: {
    port: 2700
  },
  default: {
    port: 3002
  }
}

exports.get = function get(env) {
  return config[env] || config.default;
}
